#!/usr/bin/env python
# coding: utf-8

# In[44]:


import pymongo
from IPython.display import Image
import random
import datetime
import bson
from bson.json_util import dumps
from bson.json_util import dumps
from natasha import (
    Segmenter,
    MorphVocab,
    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    NewsNERTagger,
    PER,
    NamesExtractor,
    Doc,
    DatesExtractor,
    MoneyExtractor,
    AddrExtractor
)
segmenter = Segmenter()
morph_vocab = MorphVocab()

emb = NewsEmbedding()
morph_tagger = NewsMorphTagger(emb)
syntax_parser = NewsSyntaxParser(emb)
ner_tagger = NewsNERTagger(emb)

names_extractor = NamesExtractor(morph_vocab)
dates_extractor = DatesExtractor(morph_vocab)
money_extractor = MoneyExtractor(morph_vocab)
addr_extractor = AddrExtractor(morph_vocab)


# # Вход
# ![title](img/signin.png) 
# # Карта
# ![title](img/map.png) 
# # Профиль
# ![title](img/profile.png)

# In[45]:


db = pymongo.MongoClient()
pastas = db.pastas

users = pastas.users
uscards = pastas.uscards
stories = pastas.stories
storycards = pastas.storycards


# In[46]:


verified = ''


# In[47]:


def clear():
    stories.delete_many({})
    storycards.delete_many({})
    users.delete_many({})
    uscards.delete_many({})
    
clear()


# In[48]:


def register(login, password, name, sname, email):
    '''
    Регистрирует нового пользователя. В результате создается
    документ в коллекции пользователей и в коллекции карточек
    пользователей.
    '''
    if list(users.find({"_id": login})):
        return {"response": "пользователь с таким именем уже есть"}
    else:
        users.insert_one({
            "_id": login,
            "password": password,
            "joined": datetime.datetime.utcnow(),
            "claps": 0,
            "written": 0,
            "recent_stories": [],
            "personal_info": {
                "personal": {
                    "name": name,
                    "sname": sname,
                },
                "e-mail": email,
                "telegram": "",
                "phone": "",
            },
        })
        uscards.insert_one({
            "_id": login,
            "claps": 0,
            "written": 0
        })
        return {"response": "{}, добро пожаловать!".format(login)}


# In[49]:


def sign_in(login, password):
    '''
    Принимает логин и пароль, здесь возвращает сообщение,
    но вообще меняет какое либо глобальное состояние.
    
    id залогиненого пользователя возвращается в 
    атрибуте verified_user
    '''
    access_to = list(users.find({"_id": login}))
    if access_to:
        if access_to[0]['password'] == password:
            verified = access_to[0]["_id"]
            return {"response": "Добро пожаловать, {}".format(verified), "verified_user": verified}
        else:
            return {"response": "Неверный пароль.", "verified_user": None}
    else:
        return {"response": "Нет пользователя с таким логином", "verified_user": None}


# In[50]:


def write_story_mark_spot(title, text, *tags, long=37.6, lat=55.7):
    '''
    Пишет новую историю. В результате создается документ в
    коллекции историй и в коллекции карточек с геоданными.
    '''
    ners = []
    
    doc = Doc(text)
    doc.segment(segmenter)
    doc.tag_ner(ner_tagger)
    for _ in doc.spans:
        ners.append((list(_)[3]))
    
    full_id = stories.insert_one({
        "title": title,
        "shared:": datetime.datetime.utcnow(),
        "spot": {
            "type": "Point",
            "coordinates": [
              long,
              lat
        ]},
        "comments": [],
        "select_tags": list(tags),
        "natasha_ners": ners,
        "claps": 0,
        "content": text,
        "author": verified
    }).inserted_id
    
    storycards.insert_one({
        "_id": full_id,
        "author": verified,
        "title": title,
        "select_tags": list(tags),
        "shared": datetime.datetime.utcnow(),
        "spot": {
            "type": "Point",
            "coordinates": [
              long,
              lat
        ]}
    })
    
    users.update_one(
        {"_id": verified},
        {"$inc": {"written": 1},
         "$push": {"recent_stories": {"$each":[{"link": full_id, "title": title}], "$slice": -5}}}
    )
    
    uscards.update_one(
        {"_id": verified},
        {"$inc": {"written": 1}}
    )
    return "success"


# In[51]:


def locate(bottom_left,top_right, per_page, page):
    '''
    Демонстрирует отметки с подписями в области просмотра.
    Подразумевается что прямоугольная область передается при
    перетаскивании, изменении масштаба экрана.
    '''
    respond = []
    for doc in storycards.aggregate([
        {"$match": {"spot": {"$geoWithin": {"$box": [bottom_left, top_right]}}}},
        {"$project": {"_id": 0}},
        {"$sort": {"shared": 1}},
        {"$skip": (page-1) * per_page},
        {"$limit": per_page}
    ]):
        respond.append(dumps(doc,indent=2))
    return respond


# In[52]:


def authors_list(query, per_page, page):
    '''
    Экран со списком всех пользователей, отсортированных 
    по полученным хлопкам и написанным постам
    '''
    respond = []
    for us in uscards.aggregate([
    {"$match": {"_id": {"$regex": "^{}.*".format(query)}}},
    {"$sort": {"claps": -1, "written": -1}},
    {"$skip": (page-1) * per_page},
    {"$limit": per_page}
    ]):
        respond.append(us)
    return respond


# In[53]:


def tag_stats(user):
    '''
    Возвращает информацию о кол-ве написанных постов по тегам.
    '''
    respond = []
    for stat in storycards.aggregate([
        {"$match": {"author": user}},
        {"$unwind" : "$select_tags"},
        {"$group": {"_id": "$select_tags", "total": { "$sum": 1 }}},
        {"$sort": {"total": -1}}
    ]):
        respond.append(stat)
    return respond


# In[54]:


def list_user_stories(user):
    '''
    Возвращает список историй пользователя
    '''
    respond = []
    for st in stories.aggregate([
        {"$match": {"author": user}},
        {"$project": {"title": 1, "select_tags": 1}}
    ]):
        respond.append(st)
    return respond


# In[55]:


def delete_acc():
    '''
    Удаляет пользователя и все его истории.
    '''
    users.delete_one({"_id": verified})
    uscards.delete_one({"_id": verified})
    stories.delete_many({"author": verified})
    storycards.delete_many({"author": verified})
    verified = ''
    return 'user removed'


# In[56]:


def set_bio(text):
    '''
    Добавляет био в профайл.
    '''
    users.update_one({"_id":verified},{"$set":{"bio":text}})
    return 'success'

